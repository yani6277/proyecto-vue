// import {router} from './auth.js'

const apiUrl = 'https://sails-test-api.herokuapp.com/'
const loginUrl = apiUrl + 'login/'
const registerUrl = apiUrl + 'users/'

export default {
  user: {
    message: 'por defecto',
    authenticated: false,
    token: ''
  },
  register (Vue, form) {
    return Vue.$http.post(registerUrl,
      {'email': form.email, 'password': form.password},
      {headers: {'Content-Type': 'application/json'}}
    )
  },
  login (Vue, form) {
    return Vue.$http.post(loginUrl,
      {'email': form.email, 'password': form.password},
      {headers: {'Content-Type': 'application/json'}}
    )
  }
}
